# -*- coding: utf-8 -*-

from flask import Flask, render_template
import bookdb
from flask import g, session
from flask import request, redirect, abort
from flask import flash, url_for


app = Flask(__name__)

db = bookdb.BookDB()


@app.route('/')
def books():
    books = db.titles()
    return render_template('book_list.html', entries=books)


@app.route('/book/<book_id>/')
def book(book_id):
    title = db.title_info(book_id)
    return  render_template('book_detail.html',entry = title)


if __name__ == '__main__':
    app.run('194.29.175.240',31001,debug=True)